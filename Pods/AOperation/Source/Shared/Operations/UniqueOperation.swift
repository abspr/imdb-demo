//
//  UniqueOperation.swift
//  AOperation iOS
//
//  Created by Seyed Samad Gholamzadeh on 1/8/20.
//

import Foundation

public protocol UniqueOperation {
	var uniqueId: String { get }
}
