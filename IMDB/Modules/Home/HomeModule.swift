//
//  HomeModule.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit

struct HomeModule {
    
    func build() -> UIViewController {
        let view = HomeView()
        let presenter = HomePresenter()
        view.presenter = presenter
        presenter.view = view
        return view
    }
    
}

extension HomeModule {
    enum FilterType {
        case newest, oldest, none
    }
}
