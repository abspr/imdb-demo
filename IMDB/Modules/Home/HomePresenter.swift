//
//  HomePresenter.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

protocol HomePresenterInterface {
    func viewDidLoad()
    func retryButtonDidPress()
    var numberOfItems: Int { get }
    func movie(at indexPath: IndexPath) -> Movie
    func didSelectMovie(at indexPath: IndexPath)
    func filterDidTap(type: HomeModule.FilterType)
}

class HomePresenter: NSObject {
    
    weak var view: HomeViewInterface!
    private var operationQueue = AOperationQueue()
    private var movies: [Movie] = []
    private var filteredMovies: [Movie] = []
    private var isFiltered = false
    
    private func fetchPopular() {
        let operation = GetPopularOperation()
        operation.didFinishWithResult { [weak self] (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    self?.movies = response.results
                    self?.view.stopLoading()
                    self?.view.reload()
                case .failure:
                    self?.view.showError(data: EmptyStateData.generalError)
                }
            }
        }
        operationQueue.addOperation(operation)
    }
    
}

extension HomePresenter: HomePresenterInterface {
    
    func viewDidLoad() {
        view.initialSetup()
        view.startLoading()
        fetchPopular()
    }
    
    func retryButtonDidPress() {
        view.startLoading()
        fetchPopular()
    }
    
    var numberOfItems: Int { isFiltered ? filteredMovies.count : movies.count }
    
    func movie(at indexPath: IndexPath) -> Movie { isFiltered ? filteredMovies[indexPath.row] : movies[indexPath.row] }
    
    func didSelectMovie(at indexPath: IndexPath) {
        let movie = isFiltered ? filteredMovies[indexPath.row] : movies[indexPath.row]
        let detailModule = MovieDetailModule().build(movie: movie)
        view.show(detailModule)
    }
    
    func filterDidTap(type: HomeModule.FilterType) {
        switch type {
        case .newest:
            isFiltered = true
            filteredMovies = movies.sorted(by: >)
            view.reload()
            view.filtered()
        case .oldest:
            isFiltered = true
            filteredMovies = movies.sorted(by: <)
            view.reload()
            view.filtered()
        case .none:
            isFiltered = false
            view.reload()
            view.clearFilter()
        }
    }
    
}
