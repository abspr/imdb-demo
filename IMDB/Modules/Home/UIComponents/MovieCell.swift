//
//  MovieCell.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mSubtitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: -
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    // MARK: -

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configure(with movie: Movie) {
        mTitleLabel.text = movie.title
        mSubtitleLabel.text = movie.overview
        dateLabel.text = movie.release?.date()
        posterImageView.setRemoteImage(with: movie.poster?.configurePath(width: 200))
    }
    
}
