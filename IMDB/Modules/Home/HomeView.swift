//
//  HomeView.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit
import StatefulView


protocol HomeViewInterface: class {
    func initialSetup()
    func reload()
    func startLoading()
    func stopLoading()
    func showError(data: EmptyStateData)
    func show(_ module: UIViewController)
    func clearFilter()
    func filtered()
}

class HomeView: UITableViewController {
    
    var presenter: HomePresenterInterface!
    
    
    // MARK: - Properties
    
    
    
    // MARK: - Lifecycle
    
    init() {
        super.init(style: .plain)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        
    }
    
    
    // MARK: - Setups
    
    private func setupTableView() {
        tableView.register(MovieCell.nib, forCellReuseIdentifier: MovieCell.identifier)
        tableView.rowHeight = 110
        tableView.tableFooterView = UIView()
    }
    
    private func setupFilterButton(isFiltered: Bool) {
        let image = isFiltered ? UIImage(systemName: "line.horizontal.3.decrease.circle.fill") : UIImage(systemName: "line.horizontal.3.decrease.circle")
        let filterButton = UIBarButtonItem.init(image: image,
                                                style: .plain, target: self, action: #selector(filterAction(_:)))
        navigationItem.rightBarButtonItem = filterButton
    }
    
    // MARK: - Theme
    
    
    
    // MARK: - Actions
    
    @objc private func filterAction(_ sender: UIBarButtonItem) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Show newest first", style: .default, handler: { (_) in
            self.presenter.filterDidTap(type: .newest)
        }))
        actionSheet.addAction(UIAlertAction(title: "Show oldest first", style: .default, handler: { (_) in
            self.presenter.filterDidTap(type: .oldest)
        }))
        actionSheet.addAction(UIAlertAction(title: "Clear filter", style: .destructive, handler: { (_) in
            self.presenter.filterDidTap(type: .none)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    
}

extension HomeView {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfItems
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.identifier, for: indexPath) as! MovieCell
        cell.configure(with: presenter.movie(at: indexPath))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectMovie(at: indexPath)
    }
    
}

extension HomeView: EmptyStateDelegate {
    func buttonDidPressed(_ button: UIButton) {
        presenter.retryButtonDidPress()
    }
}


extension HomeView: HomeViewInterface {
    
    func initialSetup() {
        title = "Top 20 Movies"
        navigationController?.navigationBar.prefersLargeTitles = true
        setupTableView()
        setupFilterButton(isFiltered: false)
        tableView.emptyState.delegate = self
    }
    
    func reload() {
        tableView.reloadSections([0], with: .automatic)
    }
    
    func startLoading() {
        tableView.loadState.startAnimating()
    }
    
    func stopLoading() {
        tableView.loadState.stopAnimating()
    }
    
    func showError(data: EmptyStateData) {
        tableView.emptyState.show(type: data)
    }
    
    func show(_ module: UIViewController) {
        show(module, sender: nil)
    }
    
    func clearFilter() {
        setupFilterButton(isFiltered: false)
    }
    
    func filtered() {
        setupFilterButton(isFiltered: true)
    }
    
}
