//
//  TopRatesOperation.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

class GetPopularOperation: ResultableDataOperation<PagingArrayEntity<Movie>> {
    
    init() {
        let url = Services.Movie.popular
        let request = URLRequest(url: url)
        super.init(request: request)
    }
    
}
