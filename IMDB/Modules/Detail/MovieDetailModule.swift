//
//  MovieDetailModule.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit

struct MovieDetailModule {
    
    func build(movie: Movie) -> UIViewController {
        let view = UIStoryboard(name: "MovieDetail", bundle: nil).instantiateViewController(withIdentifier: "MovieDetail") as! MovieDetailView
        let presenter = MovieDetailPresenter(movie: movie)
        view.presenter = presenter
        presenter.view = view
        return view
    }
    
}

