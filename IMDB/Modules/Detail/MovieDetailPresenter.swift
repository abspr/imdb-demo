//
//  MovieDetailPresenter.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

protocol MovieDetailPresenterInterface {
    func viewDidLoad()
}

class MovieDetailPresenter: NSObject {
    
    weak var view: MovieDetailViewInterface!
    private var movie: Movie
    private var operationQueue = AOperationQueue()
    
    init(movie: Movie) {
        self.movie = movie
    }
    
    private func fetchMovie() {
        let operation = MovieDetailOperation(id: movie.id)
        operation.didFinishWithResult { [weak self] (result) in
            DispatchQueue.main.async {
                switch result {
                case .success(let movie):
                    self?.movie = movie
                    self?.view.fill(with: movie)
                case .failure:
                    break
                }
            }
        }
        operationQueue.addOperation(operation)
    }
    
}

extension MovieDetailPresenter: MovieDetailPresenterInterface {
    
    func viewDidLoad() {
        view.initialSetup()
        view.fill(with: movie)
        fetchMovie()
    }
    
}
