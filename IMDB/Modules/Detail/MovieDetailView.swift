//
//  MovieDetailView.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit


protocol MovieDetailViewInterface: class {
    func initialSetup()
    func fill(with movie: Movie)
}

class MovieDetailView: UITableViewController {
    
    var presenter: MovieDetailPresenterInterface!
    
    
    // MARK: - Properties
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        
    }
    
}



extension MovieDetailView: MovieDetailViewInterface {
    
    func initialSetup() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    func fill(with movie: Movie) {
        posterImageView.setRemoteImage(with: movie.poster?.configurePath(width: 300))
        backgroundImageView.setRemoteImage(with: movie.background?.configurePath(width: 200))
        movieTitleLabel.text = movie.title
        rateLabel.text = "Rate: " + (movie.rate == nil ? "-" : "\(movie.rate ?? 0)")
        durationLabel.text = "Duration: " + (movie.runtime?.movieDurationFormat() ?? "-")
        dateLabel.text = "Release Date: " + (movie.release?.date() ?? "-")
        descriptionLabel.text = movie.overview
    }
    
}
