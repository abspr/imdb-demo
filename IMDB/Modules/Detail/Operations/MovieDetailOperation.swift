//
//  MovieDetailOperation.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

class MovieDetailOperation: ResultableDataOperation<Movie> {
    
    init(id: Int) {
        let url = Services.Movie.detail(id: id)
        let request = URLRequest(url: url)
        super.init(request: request)
    }
    
}
