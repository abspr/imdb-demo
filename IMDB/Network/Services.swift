//
//  Services.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

fileprivate protocol ServicesAPI {
    static var baseAddress: String { get }
    static func url(with endPath: String) -> URL
}

extension ServicesAPI {
    
    static fileprivate func url(with endPath: String) -> URL {
        return (baseAddress + endPath + "?api_key=d024973c661fb4663de92ca61ee3e581").toURL
    }
    
}


public struct Services {
    
    private static let baseDomain = "https://api.themoviedb.org/3/"
    
    // MARK: -
    
    struct Request {
        public static let timeOut: TimeInterval = 5.0
        public static let policy: URLRequest.CachePolicy = .useProtocolCachePolicy
        public static let version: String = "1"
        public static var headers: [String : String] {
            return [
                "Content-Type" : "application/json"
            ]
        }
    }
    
    
    
    // MARK: -
    
    struct app: ServicesAPI {
        static var baseAddress: String = baseDomain
        
        public static var info: URL {
            return url(with: "info")
        }
    }
    
    struct Movie: ServicesAPI {
        static var baseAddress: String = baseDomain + "movie/"
        
        public static var popular: URL {
            return url(with: "top_rated")
        }
        
        public static func detail(id: Int) -> URL {
            return url(with: "\(id)")
        }
        
    }
    
   
    
}


extension String {
    
    var toURL: URL {
        return URL(string: self)!
    }
    
}
