//
//  ResultableServicesTaskOperation.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

public class ResultableServicesTaskOperation<T: Decodable>: ResultableGroupOperation<T> {
	
	
	public func taskFinished(data: Data?, response: URLResponse?, error: Error?, finish: @escaping (AOperationError?) -> Void) {
		
		#if DEBUG
		if data != nil {
			print("json data for \(self.name ?? "") is\n\(String(data: data!, encoding: .utf8) ?? "")")
		}
		#endif
		
		
		let operation = BlockAOperation { finished in
			if let recievedData = data {
				
				self.handleRecievedData(recievedData, finish: { error in
					finish(error)
					finished()
				})
				
			}
			else {
				let error = AOperationError.executionFailed(with: [.key : self.name, .reason : "Recieved data is nil"])
				finish(error)
				finished()
			}
			
		}
		
		let errorHandlerOperation = ServicesErrorHandleOperation(operations: [operation], data: data, response: response, error: error)
		
		errorHandlerOperation.observeDidFinish { (errors) in
			finish(errors.first)
		}
		
		self.produceOperation(errorHandlerOperation)
	}
	
	open func handleRecievedData(_ data: Data, finish: @escaping (AOperationError?) -> Void) {
		DispatchQueue.main.async {
			
			var opError : AOperationError?
			
			do {
				let decoded = try JSONDecoder().decode(T.self, from: data)
				
				let result: Result<T, AOperationError> = .success(decoded)
				self.finish(with: result)
			} catch {
				print(error)
				opError = AOperationError.executionFailed(with: [AOperationError.Info(rawValue: "error"): error])
			}
			
			finish(opError)
			
		}
	}
	
	
	
}
