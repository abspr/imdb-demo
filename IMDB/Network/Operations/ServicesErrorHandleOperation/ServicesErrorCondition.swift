//
//  ErrorCondition.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

extension AOperationError {
    public func map(to type: URLError.Type) -> URLError? {
        guard (self.info?[.key] as? String) == ServicesErrorCondition.key,
            let errorCode = self.info?[ServicesErrorCondition.ErrorInfo.errorCode] as? Int,
        let errorUserInfo = self.info?[ServicesErrorCondition.ErrorInfo.errorUserInfo] as? [String : Any]
        else {  return nil }
        
        return URLError(URLError.Code(rawValue: errorCode), userInfo: errorUserInfo)
    }
}

extension ServicesErrorCondition {
    fileprivate struct ErrorInfo {
        static let errorCode = AOperationError.Info(rawValue: "errorCode")
        static let errorUserInfo = AOperationError.Info(rawValue: "errorUserInfo")
    }
}

struct ServicesErrorCondition: AOperationCondition {
    
    
    var dependentOperation: AOperation? = nil
    
    static var key: String = "ServicesError"
    
    static var isMutuallyExclusive: Bool = false
    
    let error: Error?
    
    init(for error: Error?) {
        self.error = error
    }
    
    func evaluateForOperation(_ operation: AOperation, completion: @escaping (OperationConditionResult) -> Void) {
        guard error != nil else {
            completion(.satisfied)
            return
        }

        let urlError = (self.error as? URLError)
        
        let error = AOperationError.conditionFailed(with: [
            .key : Self.key,
            ServicesErrorCondition.ErrorInfo.errorCode : urlError?.errorCode,
            ServicesErrorCondition.ErrorInfo.errorUserInfo : urlError?.errorUserInfo
        ])
        
        completion(.failed(error))

    }
    
}
