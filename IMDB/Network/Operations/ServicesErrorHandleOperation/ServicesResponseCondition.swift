//
//  ResponseCondition.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

extension AOperationError {
    
    public func map(to type: ServicesResponseCondition.Error.Type) -> ServicesResponseCondition.Error? {
        guard  (self.info?[.key] as? String) == ServicesResponseCondition.key,
            let statusCode = self.info?[ServicesResponseCondition.ErrorInfo.statusCode],
            let message = self.info?[ServicesResponseCondition.ErrorInfo.message]
        else { return nil }
        
        return ServicesResponseCondition.Error(statusCode: statusCode as! Int, message: message as? String ?? "")
    }
    
}

extension ServicesResponseCondition {
    
    fileprivate struct ErrorInfo {
        static let statusCode = AOperationError.Info(rawValue: "statusCode")
        static let message = AOperationError.Info(rawValue: "message")
    }
    
    public struct Error: Swift.Error {
        let statusCode: Int
        let message: String
    }
    
}

public struct ServicesResponseCondition: AOperationCondition {
    
    
    public var dependentOperation: AOperation? = nil
    
    
    public static var key: String = "ServicesResponse"
    
    public static var isMutuallyExclusive: Bool = false
    
    let responseStatusCode: Int?
    let data: Data?
    let url: URL?
    
    init(for response: URLResponse?, data: Data? = nil, url: URL? = nil) {
        self.responseStatusCode = (response as? HTTPURLResponse)?.statusCode
        self.data = data
        self.url = url
    }
    
    
    public func evaluateForOperation(_ operation: AOperation, completion: @escaping (OperationConditionResult) -> Void) {
        guard let statusCode = self.responseStatusCode else {
            completion(.satisfied)
            return
        }
        
        if statusCode/100 == 2 {
            completion(.satisfied)
        }
        else {
                        
            var errorReason: String? = nil
            
            
            var receivedData: Data?
            
            if let data = self.data {
                receivedData = data
            }
            else if let url = self.url {
                let data = try? Data(contentsOf: url)
                receivedData = data
            }
            
            if receivedData != nil {
                
                let dic = try? JSONSerialization.jsonObject(with: receivedData!, options: JSONSerialization.ReadingOptions()) as? [String : Any]
                
                if let detail = dic?["detail"] as? String {
                    errorReason = detail
                }
                
                if let error = dic?["error"] as? String {
                    errorReason = error
                }
                
                if let message = dic?["message"] as? String {
                    errorReason = message
                }
                
                if errorReason == nil {
                    errorReason =  (String(data: receivedData!, encoding: .utf8) ?? "")
                }
                
                #if DEBUG
                print("Response is \(statusCode) with message \(String(data: receivedData!, encoding: .utf8) ?? "")")
                #endif
                
                
            }
            
            let info: [AOperationError.Info : Any?] = [.key : Self.key, Self.ErrorInfo.statusCode : statusCode, Self.ErrorInfo.message : errorReason, .reason : errorReason, .localizedDescription : errorReason]
            
            let error = AOperationError.conditionFailed(with: info)
                        
            completion(.failed(error))
        }
        
    }
    
}
