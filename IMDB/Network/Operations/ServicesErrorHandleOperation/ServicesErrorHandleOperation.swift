//
//  ServicesErrorHandler.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

class ServicesErrorHandleOperation: GroupOperation {
    
    init(operations: [Operation], data: Data? = nil, url: URL? = nil, response: URLResponse?, error: Error?) {
        super.init(operations: operations)
        
        let responseCondition = ServicesResponseCondition(for: response, data: data, url: url)
        let errorCondition = ServicesErrorCondition(for: error)
        self.addCondition(responseCondition)
        self.addCondition(errorCondition)
    }
    
    override func finished(_ errors: [AOperationError]) {
        
        guard let error = errors.first, error.state == .conditionFailed else { return }
        let key = (error.info?[.key] as? String) ?? ""
        
        switch key {
        case ServicesResponseCondition.key:
            guard let responseError = error.map(to: ServicesResponseCondition.Error.self) else { return }
            
            self.handleResponseError(responseError)
            
        default:
            break
        }
        
    }
    
    func handleResponseError(_ responseError: ServicesResponseCondition.Error) {
        let statusCode = responseError.statusCode

        if statusCode == 401 || statusCode == 403 {
            // logout
        }
        
    }
    
}
