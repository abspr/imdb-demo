//
//  ServicesTaskOperation.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation


public class ServicesTaskOperation: GroupOperation {
    
    public func taskFinished(data: Data?, response: URLResponse?, error: Error?, finish: @escaping (AOperationError?) -> Void) {
        let operation = BlockAOperation { finished in
            if let recievedData = data {
                self.handleRecievedData(recievedData, finish: { error in
                    finish(error)
                    finished()
                })
            } else {
                let error = AOperationError.executionFailed(with: [.key : self.name, .reason : "Recieved data is nil"])
                finish(error)
                finished()
            }
            
        }
        
        let errorHandlerOperation = ServicesErrorHandleOperation(operations: [operation], data: data, response: response, error: error)
        
        errorHandlerOperation.observeDidFinish { (errors) in
            finish(errors.first)
        }
        
        self.produceOperation(errorHandlerOperation)
    }
    
    open func handleRecievedData(_ data: Data, finish: @escaping (AOperationError?) -> Void) {
        print("Override func handleRecievedData(_ data: Data, finish: @escaping (AOperationError?) -> Void) to do your job with recieved Data")
        finish(nil)
    }
    
    
}
