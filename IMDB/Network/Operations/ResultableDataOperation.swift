//
//  DataOperation.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation
import AOperation

class ResultableDataOperation<T: Decodable>: ResultableServicesTaskOperation<T> {
    
    private var request: URLRequest!
    
    init(request: URLRequest) {
        super.init(operations: [])
        self.request = request
    }
    
    override func execute() {
        self.request.allHTTPHeaderFields = Services.Request.headers
        let operation = URLSessionTaskOperation.data(for: request)
        operation.didFinish { (data, response, error, finished) in
            self.taskFinished(data: data, response: response, error: error, finish: finished)
        }
        
        self.addOperation(operation)
        
        super.execute()
    }
    
    
}

