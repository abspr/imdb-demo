//
//  PagingArrayEntity.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

struct PagingArrayEntity<T: Decodable> : Decodable {
    
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [T]
   
    enum CodingKeys: String, CodingKey {
        case page, totalPages = "total_pages", totalResults = "total_results", results
    }
}
