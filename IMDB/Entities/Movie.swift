//
//  Movie.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    let id: Int
    let title: String?
    let overview: String?
    let poster: String?
    let background: String?
    let runtime: Int?
    let rate: Double?
    let release: String?
    
    enum CodingKeys: String, CodingKey {
        case poster = "poster_path", id, title, overview, background = "backdrop_path", runtime, rate = "vote_average", release = "release_date"
    }
    
}

extension Movie: Comparable {
    static func < (lhs: Self, rhs: Self) -> Bool {
        guard let lhsDate = lhs.release?.toDate(), let rhsDate = rhs.release?.toDate() else { return false }
        return lhsDate.compare(rhsDate) == .orderedAscending
    }
    
    static func > (lhs: Self, rhs: Self) -> Bool {
        guard let lhsDate = lhs.release?.toDate(), let rhsDate = rhs.release?.toDate() else { return false }
        return lhsDate.compare(rhsDate) == .orderedDescending
    }
}
