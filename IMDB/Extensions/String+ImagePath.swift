//
//  String+Path.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

extension String {
    func configurePath(width: Int) -> String {
        return "https://image.tmdb.org/t/p/w\(width)" + self
    }
}
