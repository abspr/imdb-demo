//
//  UIImageView+extenstion.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setRemoteImage(with path: String?) {
        guard let path = path, let url = URL(string: path) else { return }
        kf.setImage(with: url, options: [.transition(.fade(0.3))])
    }
}
