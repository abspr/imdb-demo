//
//  String+Date.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

extension String {
    
    func toDate() -> Date? {
        let serverFormat = DateFormatter()
        serverFormat.dateFormat = "YYYY-MM-dd"
        guard let str = self.components(separatedBy: ".").first else { return nil }
        guard let date = serverFormat.date(from: str) else { return nil }
        return date
    }
    
    func date(format : String = "MMM d, YYYY") -> String? {
        guard let date = self.toDate() else { return nil }
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
}
