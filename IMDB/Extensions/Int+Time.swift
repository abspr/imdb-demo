//
//  Int+Time.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import Foundation

extension Int {
    
    private func toHoursMinutesSeconds() -> (hour: Int, min: Int, sec: Int) {
      return (self / 3600, (self % 3600) / 60, (self % 3600) % 60)
    }
    
    func movieDurationFormat() -> String {
        let runtime = (self * 60).toHoursMinutesSeconds()
        return "\(runtime.hour)h \(runtime.min)m"
    }
    
}
