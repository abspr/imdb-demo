//
//  EmptyStateData.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import StatefulView

enum EmptyStateData: EmptyStateType {
    case generalError
    
    var subtitle: String? { "something went wrong!" }
    var buttonTitle: String? { "RETRY" }
}
