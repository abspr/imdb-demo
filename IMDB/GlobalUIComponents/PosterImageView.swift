//
//  PosterImageView.swift
//  IMDB
//
//  Created by Hosein Abbaspoor on 6/21/1399 AP.
//  Copyright © 1399 AP Hosein Abbaspoor. All rights reserved.
//

import UIKit

class PosterImageView: UIImageView {
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        layer.cornerRadius = 8
        clipsToBounds = true
        backgroundColor = UIColor.placeholderText.withAlphaComponent(0.4)
        contentMode = .scaleAspectFill
    }
    
}
