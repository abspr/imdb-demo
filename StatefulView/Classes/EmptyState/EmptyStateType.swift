//
//  EmptyStateType.swift
//  StatefulView
//
//  Created by Hosein Abbaspour on 1/2/20.
//  Copyright © 2020 Hosein Abbaspour. All rights reserved.
//

import UIKit

public protocol EmptyStateType {
    var title: String? { get }
    var subtitle: String? { get }
    var image: UIImage? { get }
    var buttonTitle: String? { get }
}

public extension EmptyStateType {
    var title: String? { nil }
    var subtitle: String? { nil }
    var image: UIImage? { nil }
    var buttonTitle: String? { nil }
}
