//
//  EmptyStateView.swift
//  StatefulView
//
//  Created by Hosein Abbaspour on 1/2/20.
//  Copyright © 2020 Hosein Abbaspour. All rights reserved.
//

import UIKit

public class EmptyStateView: UIView {

    @IBOutlet public var titleLabel: UILabel?
    @IBOutlet public var subtitleLabel: UILabel?
    @IBOutlet public var imageView: UIImageView?
    @IBOutlet public var button: UIButton?
    @IBOutlet private var textsStack: UIStackView?
    @IBOutlet private var imageStack: UIStackView?
    @IBOutlet private var buttonStack: UIStackView?
    
    @IBOutlet internal var topConstraint: NSLayoutConstraint?
    @IBOutlet internal var verticalConstraint: NSLayoutConstraint?

    
    var tapHandler: ((UIButton) -> Void)?
    
    func fill(with type: EmptyStateType?) {
        titleLabel?.isHidden = type?.title == nil
        titleLabel?.text = type?.title
        
        subtitleLabel?.isHidden = type?.subtitle == nil
        subtitleLabel?.text = type?.subtitle
        
        imageView?.isHidden = type?.image == nil
        imageView?.image = type?.image
        
        button?.isHidden = type?.buttonTitle == nil
        button?.setTitle(type?.buttonTitle, for: .normal)
    }
    
    
    func setAppearance(_ appearance: EmptyStateAppearance) {
        titleLabel?.textColor = appearance.titleLabelColor
        subtitleLabel?.textColor = appearance.subtitleLabelColor
        button?.tintColor = appearance.buttonTintColor
        button?.backgroundColor = appearance.buttonBackgroundColor
        
        titleLabel?.textAlignment = appearance.titleTextAlignment
        subtitleLabel?.textAlignment = appearance.subtitleTextAlignment
        
        if let style = appearance.titleLabelFont { titleLabel?.font = style }
        if let style = appearance.subtitleLabelFont { subtitleLabel?.font = style}
        if let style = appearance.buttonTitleTextFont { button?.titleLabel?.font = style }
        
        button?.contentEdgeInsets = appearance.buttonContentInset
        button?.layer.cornerRadius = appearance.buttonCornerRadius
        
        textsStack?.spacing = appearance.textsVerticalSpace
        imageStack?.spacing = appearance.imageBottomInset
        buttonStack?.spacing = appearance.buttonTopInset
    }
    
    
    public override func awakeFromNib() {
        button?.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
    }
    
    
    @objc private func buttonAction(_ sender: UIButton) {
        tapHandler?(sender)
    }

}



extension EmptyStateView {
    static func fromNib(_ nib: UINib? = nil) -> EmptyStateView {
        if let nib = nib {
            let v = nib.instantiate(withOwner: nil, options: nil).last
            if !(v is EmptyStateView) {
                fatalError("Could not cast your custom nib to EmptyStateView. Make sure you set the nib's view subclass to EmptyStateView")
            }
            return v as! EmptyStateView
        }
        let nibName = String(describing: self)
        let bundle = Bundle(for: self.classForCoder())
        return UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: nil, options: nil).last as! EmptyStateView
    }
}
