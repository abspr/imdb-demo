//
//  LoadState.swift
//  StatefulView
//
//  Created by Hosein Abbaspour on 1/1/20.
//  Copyright © 2020 Hosein Abbaspour. All rights reserved.
//

import UIKit

/**
 Activity indicator for any UIView.
 
 In most cases you will not use this class directly. This is how you will probably use:
 
 ```
 yourView.loadState.startAnimating()
 ```
 and
 ```
 yourView.loadState.stopAnimating()
 ```
 */
open class LoadState {
    
    // MARK: - public variables
    
    /// A boolean value determines whether activity indicator is hidden or not.
    public private(set) var isAnimating: Bool = false
    
    /// Set this to your own custom struct conforms to LoadStateAppearance protocol for customized activity indicator
    public var appearance: LoadStateAppearance! {
        didSet {
            indicator.color = appearance.color
            indicator.style = appearance.style
        }
    }
    
    
    // MARK: - private variables
    
    
    private var indicator: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView()
        activity.translatesAutoresizingMaskIntoConstraints = false
        activity.hidesWhenStopped = true
        return activity
    }()
    
    private var buttonTitle: String?
    private var buttonImage: UIImage?
    
    
    private var parent: UIView!
    
    private var verticalConstraint: NSLayoutConstraint!
    
    // MARK: - init
    
    /// - Parameter view: UITableView, UICollectionView or just any UIView that will be emptyState parent.
    init(in view: UIView) {
        parent = view
        view.addSubview(indicator)
        verticalConstraint = indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -view.safeAreaInsets.top / 2)
        verticalConstraint.isActive = true
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    
    
    // MARK: - public methods
    
    /// Animating activity indicator view
    public func startAnimating(verticalOffset: CGFloat = 0) {
        if parent.emptyState.isVisible { parent.emptyState.hide() }
        isAnimating = true
        if let btn = parent as? UIButton {
            buttonTitle = btn.title(for: .normal)
            buttonImage = btn.image(for: .normal)
            btn.setImage(nil, for: .normal)
            btn.setTitle(nil, for: .normal)
            btn.isUserInteractionEnabled = false
        }
        indicator.startAnimating()
        if let view = parent as? UICollectionView {
            if let headerHeight = (view.collectionViewLayout as? UICollectionViewFlowLayout)?.headerReferenceSize.height {
                verticalConstraint.constant = headerHeight / 2 - view.safeAreaInsets.top / 2 + verticalOffset
            }
        } else { verticalConstraint.constant = verticalOffset }
    }
    
    /// Stop animating activity indicator view
    public func stopAnimating() {
        isAnimating = false
        indicator.stopAnimating()
        if let btn = parent as? UIButton {
            btn.setImage(buttonImage, for: .normal)
            btn.setTitle(buttonTitle, for: .normal)
            btn.isUserInteractionEnabled = true
        }
    }
    
    
    
}
